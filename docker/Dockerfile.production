ARG DEBUG=false
ARG VERSION=0.0.0
ARG DEPLOYMENT=production
ARG MAINTANER="ludd@food.dtu.dk"

# 1 Builder Image
FROM node:13.8.0-alpine3.10 AS builder

ARG DEPLOYMENT
ENV DEPLOYMENT=$DEPLOYMENT

WORKDIR /app

COPY . /app

RUN yarn install
RUN npm run build

# 2 Production Image
FROM nginx:1.17.8-alpine AS deploy

# Labels intended to be populated on image build
ARG VERSION
LABEL version=$VERSION
ARG GIT_COMMIT
LABEL git_commit=$GIT_COMMIT
ARG BUILD_DATE
LABEL build_date=$BUILD_DATE
ARG MAINTANER
LABEL maintainer=$MAINTANER
ARG DEPLOYMENT
LABEL deployment=$DEPLOYMENT

RUN apk add --no-cache curl

WORKDIR /usr/share/nginx/html

RUN rm -r /usr/share/nginx/html/*
RUN rm -f /etc/nginx/conf.d/default.conf

COPY --from=builder /app/dist/ /usr/share/nginx/html
COPY --from=builder /app/config/services/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/config/services/app.nginx.production.conf /etc/nginx/conf.d/app.conf
