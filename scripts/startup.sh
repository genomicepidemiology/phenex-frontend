#!/usr/bin/env ash

ln -s /app/scripts/aliases.sh /etc/profile.d/aliases.sh

# NGINX
rm -f /etc/nginx/conf.d/default.conf
cp /app/config/services/nginx.conf /etc/nginx/nginx.conf

if [ "$DEPLOYMENT" = "development" ]; then
  cp /app/config/services/app.nginx.development.conf /etc/nginx/conf.d/app.conf

  # NodeJS
  yarn install
  yarn run build
  cp -r /app/dist/* /usr/share/nginx/html
  npm run serve &
fi

if [ "$DEPLOYMENT" = "testing" ]; then
  cp /app/config/services/app.nginx.testing.conf /etc/nginx/conf.d/app.conf

  # NodeJS
  yarn install
  yarn run build
  cp -r /app/dist/* /usr/share/nginx/html
fi

if [ "$DEPLOYMENT" = "production" ]; then
  cp /app/config/services/app.nginx.production.conf /etc/nginx/conf.d/app.conf

  # NodeJS
  yarn install
  yarn run build
  cp -r /app/dist/* /usr/share/nginx/html
fi

# START WEB_SERVER IN FOREGROUND
nginx -g "daemon off;"
