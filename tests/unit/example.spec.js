import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Alert from '@/components/Alert.vue'

// TODO: Fix Test!!!!
describe('Alert.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(Alert, {
      propsData: { alert }
    })
    expect(wrapper.text()).to.include(msg)
  })
})
