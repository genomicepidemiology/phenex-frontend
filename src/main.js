import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import store from './store'
import storage from './utilities/storage.js'
import Authenticator from './utilities/authenticator.js'
import jQuery from 'jquery/dist/jquery.slim.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'

// TODO: Get rid of jQuery
global.$ = jQuery

Vue.config.productionTip = false
Vue.prototype.$appName = 'Phenex Web'

Vue.use(VueAxios, Axios)

var auth = new Authenticator()
auth.loggInFromStorage()
// console.log('ggg', Vue.prototype.axios.defaults.headers.common.Authorization)

new Vue({
  router,
  store,
  storage,
  render: h => h(App)
}).$mount('#app')
