import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: false,
  state: {
    user: {
      data: {},
      loggedIn: false,
      accessToken: ''
    }
  },
  getters: {
    data: function (state) {
      return state.user.data
    }
  },
  mutations: {
    setUserData: function (state, data) {
      state.user.data = data
    },
    setUserAccessToken: function (state, token) {
      state.user.accessToken = token
    },
    setUserLoggedInFlag: function (state, flag = false) {
      state.user.loggedIn = flag
    }
  },
  actions: {},
  modules: {}
})
